# WordPress Websites

These WordPress websites are projects that I have worked on during my employment at Drama Queen Communications.  
In some of them, I have been the lead developer, while in others, I have served as an assistant developer.

All of these websites are based on a WordPress theme created by our team which we then have customized for each project. The websites use Gutenberg block editor and a few plugins such as ACF Pro, Polylang, Yoast SEO, Gravity Forms and Safe SVG.

## Lead

[MLL Varsinais-Suomen piiri ry](https://varsinaissuomenpiiri.mll.fi/)\
[Kultaranta Resort](https://kultarantaresort.fi/)\
[Herkkupiste](https://www.herkkupiste.fi/)\
[Saipu](https://saipu.fi/)\
[Apteekki Sastamala](https://apteekkisastamala.fi/)\
[Lundén Foods Pro](https://lundenfoodspro.fi/)\
[Lounais-Suomen kirjastojen extranet](https://ls-kirjastot.fi/)



## Assistant

[Ecom](https://www.ecom.fi/)\
[Noja Rahoitus](https://noja.fi/)
